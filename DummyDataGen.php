<?php

class DummyDataGenerator{
	const ACTION_NONE = 0;
    const ACTION_POST = 1;
    const ACTION_SHARE = 2;
    const ACTION_LIKE = 3;
    const ACTION_COMMENT = 4;

    function __construct($a){
        $this->args = $a;
        array_shift($this->args);
    }

    function main(){
        if ( count($this->args) == 0 ) return;
        // Twitter
        for($i=0; $i < rand(5,50); $i++){
            echo $this->formatAsSql($this->generateTwitter($this->args[0]), "\n");
        }

        for($i=0; $i < rand(5,50); $i++){
            echo $this->formatAsSql($this->generateFacebook($this->args[0]), "\n");
        }
    }

    function generateTwitter($id){
        $data = $this->makeBaseData($id);
        $data['key'] = $this->generateKey(); 
        $data['PlatformId'] = 103; 
        $data['SocialActionTypeId'] = rand(1,4); 
        $data['actor'] = '@' . $this->getRandomString(); 
        return $data;        
    }
    function generateFacebook($id){
        $data = $this->makeBaseData($id);
        $data['key'] = $this->generateKey(); 
        $data['PlatformId'] = 100; 
        $data['SocialActionTypeId'] = rand(1,4); 
        $data['actor'] = rand(1,99990) . rand(0,99999) . rand(0,999); 
        return $data;        
    }

    function generateTumblr($id){

        $data = $this->makeBaseData($id);
        $data['key'] = $this->generateKey(); 
        $data['PlatformId'] = 104; 
        $data['SocialActionTypeId'] = rand(1,4); 
        $data['actor'] = $this->getRandomString(9); 
        return $data;        
    }

    function generatePinterest($id){
        $data = $this->makeBaseData($id);
        $data['key'] = $this->generateKey(); 
        $data['PlatformId'] = 105; 
        $data['SocialActionTypeId'] = rand(1,4); 
        $data['actor'] = $this->getRandomString(9); 
        return $data;        
    }

    function generateInstagram($id){
        $data = $this->makeBaseData($id);
        $data['key'] = $this->generateKey(); 
        $data['PlatformId'] = 101; 
        $data['SocialActionTypeId'] = rand(1,4); 
        $data['actor'] = $this->getRandomString(9); 
        return $data;        
    }

    function generateGoogleplus($id){
        $data = $this->makeBaseData($id);
        $data['key'] = $this->generateKey(); 
        $data['PlatformId'] = 102; 
        $data['SocialActionTypeId'] = rand(1,4); 
        $data['actor'] = rand(1,99990) . rand(0,99999) . rand(0,999); 
        return $data;        
    }

    function generateKey(){
        $s = substr(md5(time()), 0, 8);
        
        $randomString = $s . rand(1000,9999) ;
        return "http://scooprmedia.com/a/b/" . $randomString;
    }

    function getRandomString($length = 6) {
        $validCharacters = "abcdefghijklmnopqrstuxyvwz0123456789_";
        $validCharNumber = strlen($validCharacters);
     
        $result = "";
     
        for ($i = 0; $i < $length; $i++) {
            $index = mt_rand(0, $validCharNumber - 1);
            $result .= $validCharacters[$index];
        }
     
        return $result;
    }

    function makeBaseData($id){
        $data['sdate'] = strftime('%Y-%m-%d %T'); 
        $data['created'] = strftime('%Y-%m-%d %T'); 
        $data['modified'] = strftime('%Y-%m-%d %T'); 
        $data['key'] = null; 
        $data['PlatformId'] = null; 
        $data['SocialActionTypeId'] = self::ACTION_NONE; 
        $data['actor'] = null; 
        $data['AssignmentImgSubmissionId'] = $id;
        return $data;
    }

    function formatAsSql($data, $c=null){

        $s = sprintf("INSERT INTO `Interaction` (`sdate`, `created`, `modified`, `key`, `PlatformId`, `SocialActionTypeId`, `actor`, `AssignmentImgSubmissionId`)
VALUES
    ('%s', '%s', '%s', '%s', %d, %d, '%s', %d);
    ", 
        $data['sdate'], $data['created'], $data['modified'], $data['key'], $data['PlatformId'], 
        $data['SocialActionTypeId'], $data['actor'], $data['AssignmentImgSubmissionId']);
        if ( $c ) $s .= $c;
        return $s;
    }

}
$a = new DummyDataGenerator($argv);
$a->main();

/*
INSERT INTO `Interaction` (`InteractionId`, `sdate`, `created`, `modified`, `key`, `PlatformId`, `SocialActionTypeId`, `actor`, `AssignmentImgSubmissionId`)
VALUES
	(1030, '2014-01-02 00:00:00', '2014-01-02 00:00:00', '2014-01-02 00:00:00', 'scooprmedia.com/i/aa12341', 103, 3, '@xxxxxxxxxx0336', 292);
*/




